Pod::Spec.new do |spec|
    spec.name             = 'eztokensdk'
    spec.version          = '1.3.0'
    spec.summary          = 'EZ Token SDK for iOS devices'
    spec.homepage         = 'http://developers.eezeepay.net/token_sdk_ios/1.3.0/'
    spec.license          = { :type => 'Copyright', :text => '2018 The Payment House. All rights reserved.' }
    spec.author          = { 'Ivan Vishnev' => 'ivan@thepaymenthouse.com'}

    spec.platform = :ios
    spec.ios.deployment_target = '10.0'
    
    spec.source = {:http => 'https://bitbucket.org/tphdev/pods/downloads/eztokensdkv1.3.zip'}
  
    spec.ios.vendored_frameworks = 'eeZeeTokenSDK_iOS.framework'
    spec.framework = 'CoreLocation'
    spec.dependency 'CocoaLumberjack', '~> 3.2'
    spec.dependency 'Lockbox', '~> 2.1.0'
    spec.requires_arc     = true
end