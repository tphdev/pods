Pod::Spec.new do |spec|
    spec.name             = 'ezpossdk'
    spec.version          = '1.4.0'
    spec.summary          = 'EZ POS SDK for iOS devices'
    spec.license            = { :type => 'Copyright', :text => '©2018 The Payment House. All rights reserved.' }

    spec.description      = 'This document describes the interfaces between merchant applications and the eeZee Pay Server for the purpose of enabling Merchants from processing electronic payments through The Payment House’spec payment platform in their own applications and cash registers.'
                       

    spec.homepage         = 'http://developers.eezeepay.net/pos_sdk_ios/1.4.0/'
    spec.authors          = { 'Ivan Vishnev' => 'ivan@thepaymenthouse.com', 'Alexander Grischenko' => 'al.grischenko@gmail.com' }

    spec.ios.deployment_target = '10.0'
    spec.requires_arc     = true

    spec.source = {:http => 'https://bitbucket.org/tphdev/pods/downloads/ezpossdkv1.4.zip'}
  
    spec.subspec 'pos' do |pos|

        pos.subspec 'sdk' do |sdk|
            sdk.vendored_frameworks    = 'eeZeePosSDK_iOS.framework'
            sdk.dependency               'ezpossdk/pos/internal/core'
            sdk.dependency               'ezpossdk/pos/internal/payment'
        end

        pos.subspec 'internal' do |internal|

            internal.subspec 'core' do |core|
                core.vendored_frameworks = 'TphCore_iOS.framework'
            end

            internal.subspec 'payment' do |payment|
                payment.vendored_frameworks = 'TphPaymentProcessor_iOS.framework'
                payment.frameworks = 'CoreLocation'
                payment.resource = 'mpos.core-resources.bundle'
                payment.resource = 'mpos-ui-resources.bundle'
                payment.dependency 'CocoaLumberjack', '~> 3.2'
                payment.dependency 'Lockbox', '~> 2.1.0'
            end

        end
    end
end